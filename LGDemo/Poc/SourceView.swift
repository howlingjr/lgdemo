//
//  SourceAudioView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 19/12/2021.
//

import SwiftUI
import Combine

struct SourceView: View {
    
    let dropboxViewModel = SourceAudioViewModel(source: DropboxSource())
    let googleDriveViewModel = SourceAudioViewModel(source: GoogleDriveSource())
    @State var isDropbox: Bool = true
    
    var body: some View {
        VStack(spacing: 100) {
            Button(action: {
                isDropbox.toggle()
            }) {
                NavigationLink(destination: SourceAudioView(viewModel: isDropbox ? dropboxViewModel: googleDriveViewModel)) {
                    Text("Show audio")
                }
            }
            Button("Change source audio", action: { isDropbox.toggle() })
        }
    }
}

struct SourceAudioView: View {
    /*
     Une instance @ObservedObject est remplacée chaque fois que SwiftUI décide de supprimer et de redessiner la vue,
     et vous pouvez donc rencontrer des plantages étranges dans différents scénarios.En utilisant @StateObject,
     vous vous assurez que l'instance que nous créons est conservée même lorsque la vue est redessinée ou supprimée
     */
    
    //@ObservedObject var viewModel: SourceAudioViewModel
    @StateObject  var viewModel: SourceAudioViewModel
    
    var body: some View {
        VStack {
            Text(viewModel.textToShow).font(.system(.largeTitle)).padding()
            Button("Randomize tracks", action: { self.viewModel.getTrack() })
        }
    }
}

protocol Source {
    // Wrapped value
    var name: String { get }
    
    // (Published property wrapper)
    var namePublished: Published<String> { get }
    
    // Publisher
    var namePublisher: Published<String>.Publisher { get }
    func getTrack()
}

class DropboxSource: Source, ObservableObject {
    
    @Published private(set) var name: String = ""
    var namePublished: Published<String> { _name }
    var namePublisher: Published<String>.Publisher { $name }
    
    let files = ["dropboxTrack1", "dropboxTrack2"]
    
    init() {
        getTrack()
    }
    
    func getTrack() {
        self.name = files.randomElement() ?? ""
    }
}

class GoogleDriveSource: Source, ObservableObject {
    
    @Published private(set) var name: String = ""
    var namePublished: Published<String> { _name }
    var namePublisher: Published<String>.Publisher { $name }
    
    let files = ["googleDrivetrack1", "googleDrivetrack2"]
    
    init() {
        getTrack()
    }
    
    func getTrack() {
        self.name = files.randomElement() ?? ""
    }
}

class SourceAudioViewModel: ObservableObject {
    
    @Published var textToShow: String = "No data"
    var source: Source
    var cancellable: AnyCancellable?
    
    init(source: Source) {
        self.source = source
        cancellable =
        source
            .namePublisher
            .receive(on: RunLoop.main)
            .sink { [weak self] data in
                self?.textToShow = data
            }
    }
    
    func getTrack() {
        source.getTrack()
    }
}

struct SourceView_Previews: PreviewProvider {
    static var previews: some View {
        SourceView()
    }
}
