//
//  GenericArrayView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 17/12/2021.
//

import SwiftUI

struct BasicItem: Hashable, CustomStringConvertible {
    var description: String
    var id: Int
}

struct DeepItem: Hashable, CustomStringConvertible {
    var description: String
    var id: Int
    var name: String
}

struct GenericArrayView: View {
    @State private var basicArray: [BasicItem] = [BasicItem(description: "Apple", id: 0), BasicItem(description: "Android", id: 1)]
    @State private var deepArray: [DeepItem] = [DeepItem(description: "Apple", id: 0, name: "good"), DeepItem(description: "Android", id: 1, name: "bad")]


    var body: some View {
        VStack {
        CustomView(array: $basicArray)
        CustomView(array: $deepArray)
        }
    }
}

struct CustomView<T: Hashable & CustomStringConvertible>: View {
    @Binding var array: [T]
    
    var body: some View {
        VStack {
            ForEach(array, id:\.self) { item in
                Text(String(describing: item))
            }
        }
    }
}

struct GenericArrayView_Previews: PreviewProvider {
    static var previews: some View {
        GenericArrayView()
    }
}
