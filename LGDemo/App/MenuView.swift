//
//  ContentView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 13/11/2021.
//

import SwiftUI

struct MenuView: View {
    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Indie")) {
                    NavigationLink("Custom font", destination: CustomFontView())
                    NavigationLink("Activity indicator", destination: EmptyView())
                    NavigationLink("Alert", destination: EmptyView())
                    NavigationLink("Neumorphism", destination: EmptyView())
                }
                
                Section(header: Text("Basics")) {
                    NavigationLink("Text", destination: TextView())
                    NavigationLink("Label", destination: LabelView())
                    NavigationLink("Button", destination: ButtonView())
                }
                
                Section(header: Text("Poc")) {
                    NavigationLink("Generic array", destination: GenericArrayView())
                    NavigationLink("Source", destination: SourceView())
                }
                
            }.navigationBarTitle(Text("LG DEMO"))
        }
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
