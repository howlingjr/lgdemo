//
//  LGDemoApp.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 13/11/2021.
//

import SwiftUI

@main
struct LGDemoApp: App {
    var body: some Scene {
        WindowGroup {
            MenuView().themeStyle(ExampleStyle())
        }
    }
}
