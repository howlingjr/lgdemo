//
//  Fonts.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 14/11/2021.
//

import SwiftUI

public struct ExampleStyle: ViewModifier {
    public func body(content: Content) -> some View {
        content
        /// Fonts
        // TODO:     add relative to ? ->  Font.custom("HelveticaNeue-Bold", size: 20, relativeTo: .title2)
        
            .environment(\.h1,  Font.custom("AvenirNext-UltraLightItalic", size: 20))
            .environment(\.h2,  Font.custom("BradleyHandITCTT-Bold", size: 20))
            .environment(\.h3,  Font.custom("Avenir-Black", size: 50))
            .environment(\.h4,  Font.custom("HelveticaNeue-Bold", size: 10))
        
        /// Colors
            .environment(\.lightPrimary,  Color(red: 122, green: 33, blue: 144))
            .environment(\.darkPrimary,  Color(hex: 0xf5bc53, opacity: 0.4))
        
        /// Constraints
            .environment(\.margin, 32.0)
            .environment(\.buttonHeight, 57)
        
        
        
    }
}

