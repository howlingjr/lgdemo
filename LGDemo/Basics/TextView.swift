//
//  TextView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 21/11/2021.
//

import SwiftUI
import LGSwiftUI

struct TextView: View {
    var body: some View {
        VStack (spacing : 10) {
            Text("H1 style").textStyle(H1Style())
            Text("H2 style").textStyle(H2Style())
            Text("H3 style").textStyle(H3Style())
            Text("H4 style").textStyle(H4Style())
        }
    }
}

struct TextView_Previews: PreviewProvider {
    static var previews: some View {
        TextView()//.environment(\.colorScheme, .dark)
    }
}
