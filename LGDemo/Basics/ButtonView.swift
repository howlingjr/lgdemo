//
//  ButtonView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 21/11/2021.
//

import SwiftUI
import LGSwiftUI

struct ButtonView: View {
    @Environment(\.margin) var margin
    
    var body: some View {
        GeometryReader { geometry in
            Button("Next") {
                print("Next")
            }.buttonStyle(PrimaryButtonStyle(maxWidth: geometry.size.width - margin*2))
        }
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView()
    }
}
