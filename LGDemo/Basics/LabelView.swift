//
//  LabelView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 14/11/2021.
//

import SwiftUI
import LGSwiftUI

struct LabelView: View {

    var body: some View {
        VStack (spacing : 10) {
        Label("bus", systemImage: "bus")
        Label("Label Wi-Fi avec style", systemImage: "wifi").labelStyle(CustomIconLabelStyle(color: .orange, size: 1))
        }

    }
}

struct LabelView_Previews: PreviewProvider {
    static var previews: some View {
        LabelView()
    }
}
