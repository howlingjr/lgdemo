//
//  CustomFontView.swift
//  LGDemo
//
//  Created by Ludovic Grimbert on 13/11/2021.
//

import SwiftUI
import CustomFont


struct CustomFontView: View {
    var body: some View {
        Text("Hello, World!")
            .customFont(name: "Helvetica Neue", style: .title2, weight: .light)
    }
}

struct CustomFontView_Previews: PreviewProvider {
    static var previews: some View {
        CustomFontView()
    }
}
